#include <iostream>
#include "toString.h"
#include <fstream>
class My
{
public:
	int a;
	My(int r):a(r){};

	friend std::ostream& operator << (std::ostream& out,const My& my){
		out << my.a;
	}
};

int main(){
	int a = 0;
	My m(10);

	std::cout << toString(m) << std::endl;
	std::cout << toString(10) << std::endl;
	std::cout << toString(3.14) << std::endl;
	std::cout << toString("Hello world!") << std::endl;


	return 0;
}