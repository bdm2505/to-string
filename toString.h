#ifndef TO_STRING
#define TO_STRING 

#include <string>
#include <sstream>

namespace bdm{

struct divmod10_t
{
    int quot;
    char rem;
};
inline divmod10_t divmodu10(int n)
{
    divmod10_t res;
// умножаем на 0.8
    res.quot = n >> 1;
    res.quot += res.quot >> 1;
    res.quot += res.quot >> 4;
    res.quot += res.quot >> 8;
    res.quot += res.quot >> 16;
    int qq = res.quot;
// делим на 8
    res.quot >>= 3;
// вычисляем остаток
    res.rem = char(n - ((res.quot << 1) + (qq & ~7ul)));
// корректируем остаток и частное
    if(res.rem > 9)
    {
        res.rem -= 10;
        res.quot++;
    }
    return res;
}

char * utoa_fast_div(int value, char *buffer)
{
    buffer += 11;
    *--buffer = 0;
    do
    {
        divmod10_t res = divmodu10(value);
        *--buffer = res.rem + '0';
        value = res.quot;
    }
    while (value != 0);
    return buffer;
}

char *toChars(int i){
	if(i>=0){
		return utoa_fast_div(i,new char[10]);
	} else{
		char* chars = new char[11];
		chars[0] = '-';
		utoa_fast_div(-i,chars+1);
		return chars;
	}
}
}

std::string toString(int i){
	std::string s(bdm::toChars(i));
	return s;
}
template <typename T> 
std::string toString(T t){
	std::stringstream out;
	out << t;
	return out.str();
}


#endif